/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.app.electronicStudy;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.app.electronicStudy";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 17;
  public static final String VERSION_NAME = "electronicstudy.17.0.20191007";
}
