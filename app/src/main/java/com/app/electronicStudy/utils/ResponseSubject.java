package com.app.electronicStudy.utils;

/**
 * Created by srishti on 20/08/19.
 */

public class ResponseSubject {
    public static class ResultEntity {
        private String id;
        private String subjectName;
        private String courseId;

        public void setId(String id){this.id = id;}
        public String getId(){return id;}

        public void setSubjectName(String subjectName){this.subjectName = subjectName;}
        public String getSubjectName(){return subjectName;}

        public void setCourseId(String courseId){this.courseId = courseId;}
        public String getCourseId(){return courseId;}
    }
}
