package com.app.electronicStudy.utils;

/**
 * Created by srishti on 14/08/19.
 */

public class Constants {

    public static final String USER_ID = "user_id";
    public static final String USER_NAME = "user_name";
    public static final String USER_CONTACT = "user_contact";
    public static final String USER_EMAIL = "user_email";
    public static final String IS_LOGGED_IN = "is_user_logged_in";
    public static final String serverUrl = "http://electronicstudy.in/api/electronic_study_api.php";
    public static final String getPdfUrl = "http://electronicstudy.in/files/";

    public static final String getHashKeyUrl = "http://electronicstudy.in/payment/files/new_hash.php";
    public static final String generateChecksumUrl = "http://electronicstudy.in/payment/generateChecksum.php";

    public static final String FILE_TYPE_SINGLE = "Single File";
    public static final String FILE_TYPE_MULTIPLE = "Multiple File";

    public static final String paytmMerchantId = "xUzWzP93722355639394";

}
