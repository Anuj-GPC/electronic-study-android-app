package com.app.electronicStudy.utils;

/**
 * Created by srishti on 05/10/19.
 */

public class ResponseMultiListPdf {

    public static class ResultEntity {
        private String pdfId;
        private String parentId;
        private String pdfName; // display name
        private String pdfFileName;


        public void setPdfId(String pdfId){this.pdfId = pdfId;}
        public String getPdfId(){return pdfId;}

        public void setPdfName(String pdfName){this.pdfName = pdfName;}
        public String getPdfName(){return  pdfName;}

        public void setParentId(String parentId){this.parentId = parentId;}
        public String getParentId(){return parentId;}

        public void setPdfFileName(String pdfFileName){this.pdfFileName = pdfFileName;}
        public String getPdfFileName(){return  pdfFileName;}
    }
}
