package com.app.electronicStudy.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.electronicStudy.PdfGridAdapter;
import com.app.electronicStudy.R;

import java.util.ArrayList;

/**
 * Created by srishti on 05/10/19.
 */

public class MultiPdfGridAdapter extends BaseAdapter {
    private final LayoutInflater mInflator;
    Context mContext;
    private ArrayList<ResponseMultiListPdf.ResultEntity> mPdfList;

    public MultiPdfGridAdapter(Context context, ArrayList<ResponseMultiListPdf.ResultEntity> mData) {

        this.mContext = context;
        this.mPdfList = mData;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mPdfList.size();
    }

    @Override
    public ResponseMultiListPdf.ResultEntity getItem(int position) {
        return mPdfList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MultiPdfGridAdapter.ViewHolder holder;

        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.pdf_gv_item, null);
            holder = new MultiPdfGridAdapter.ViewHolder();
            holder.pdfTextView = (TextView) convertView.findViewById(R.id.pdf_name_tv);
            holder.pdfImageView = (ImageView) convertView.findViewById(R.id.pdf_img);

            convertView.setTag(holder);
        } else {
            holder = (MultiPdfGridAdapter.ViewHolder) convertView.getTag();
        }
        ResponseMultiListPdf.ResultEntity resultEntity = getItem(position);
        holder.pdfTextView.setText(resultEntity.getPdfName());
        return convertView;

    }

    private class ViewHolder {
        private ImageView pdfImageView;
        private TextView pdfTextView;
    }
}
