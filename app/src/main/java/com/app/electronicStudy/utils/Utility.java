package com.app.electronicStudy.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by srishti on 14/08/19.
 */

public class Utility {

    public static void putStringValueInSharedPreference(Context context,
                                                        String key, String value) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    public static String getStringValueFromSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return preferences.getString(param, "");
    }


    public static void putIntValueInSharedPreference(Context context,
                                                     String key, int value) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }
    public static int getIntValueFromSharedPreference(Context context, String param) {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        return preferences.getInt(param,0);
    }



}
