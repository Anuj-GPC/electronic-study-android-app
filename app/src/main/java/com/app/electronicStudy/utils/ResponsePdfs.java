package com.app.electronicStudy.utils;

/**
 * Created by srishti on 24/08/19.
 */

public class ResponsePdfs {

    public static class ResultEntity {
        private String id;
        private String pdfName;
        private String subjectId;
        private String pdfAmount;
        private String pdfNameToDisplay;
        private String pdfFileType;

        public void setId(String id){this.id = id;}
        public String getId(){return id;}

        public void setPdfName(String pdfName){this.pdfName = pdfName;}
        public String getPdfName(){return  pdfName;}

        public void setSubjectId(String subjectId){this.subjectId = subjectId;}
        public String getSubjectId(){return subjectId;}

        public void setPdfAmount(String pdfAmount){this.pdfAmount = pdfAmount;}
        public String getPdfAmount(){return pdfAmount;}

        public void setPdfNameToDisplay(String pdfNameToDisplay){this.pdfNameToDisplay = pdfNameToDisplay;}
        public String getPdfNameToDisplay(){return  pdfNameToDisplay;}

        public void setPdfFileType(String pdfFileType){this.pdfFileType = pdfFileType;}
        public String getPdfFileType(){return pdfFileType;}
    }

    public static class PaymentEntity {
        private String pdfId;
        private int paymentStatus;

        public void setPdfId(String pdfId){this.pdfId = pdfId;}
        public String getPdfId(){return pdfId;}

        public void setPaymentStatus(int paymentStatus){this.paymentStatus = paymentStatus;}
        public int getPaymentStatus(){return paymentStatus;}
    }

}

