package com.app.electronicStudy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener{

    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    EditText passTv, newPassTv, reEnterPassTv;
    private CheckBox passChk, newPassChk, reenterPassChk;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        userId = Utility.getStringValueFromSharedPreference(this, Constants.USER_ID);
        setLayoutId();
        setOnClickListeners();
    }

    private void setOnClickListeners(){
        findViewById(R.id.change_pass_btn).setOnClickListener(this);

        passChk.setOnClickListener(this);
        newPassChk.setOnClickListener(this);
        reenterPassChk.setOnClickListener(this);
    }

    private void setLayoutId(){
        passTv = (EditText) findViewById(R.id.old_pass_et);
        newPassTv = (EditText) findViewById(R.id.new_pass_et);
        reEnterPassTv = (EditText) findViewById(R.id.reenter_pass_et);
        passChk = (CheckBox) findViewById(R.id.old_pass_chk);
        newPassChk = (CheckBox) findViewById(R.id.new_pass_chk);
        reenterPassChk = (CheckBox) findViewById(R.id.reenter_pass_chk);

    }

    private boolean validateData() {
        if (TextUtils.isEmpty(passTv.getText().toString())){
            Toast.makeText(this, "Entering old password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(newPassTv.getText().toString())){
            Toast.makeText(this, "Entering new password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (newPassTv.getText().toString().length() < 6){
            Toast.makeText(this, "Password should be of minimum 6 characters", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(reEnterPassTv.getText().toString())){
            Toast.makeText(this, "Re-entering new password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        }else if (!(reEnterPassTv.getText().toString().equals(newPassTv.getText().toString()))) {
            Toast.makeText(this, "New Password and re enter password dont match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.change_pass_btn:
                if(validateData()){
                    callChangePasswordApi(passTv.getText().toString(),reEnterPassTv.getText().toString());
                }
                break;
            case R.id.old_pass_chk:
                if(passChk.isChecked()){
                    passTv.setTransformationMethod(new HideReturnsTransformationMethod());
                }
                else{
                    passTv.setTransformationMethod(new PasswordTransformationMethod());
                }
                passTv.setSelection(passTv.getText().length());

                break;
            case R.id.new_pass_chk:
                if(newPassChk.isChecked()){
                    newPassTv.setTransformationMethod(new HideReturnsTransformationMethod());
                }
                else{
                    newPassTv.setTransformationMethod(new PasswordTransformationMethod());
                }
                newPassTv.setSelection(newPassTv.getText().length());

                break;
            case R.id.reenter_pass_chk:
                if(reenterPassChk.isChecked()){
                    reEnterPassTv.setTransformationMethod(new HideReturnsTransformationMethod());
                }
                else{
                    reEnterPassTv.setTransformationMethod(new PasswordTransformationMethod());
                }
                reEnterPassTv.setSelection(reEnterPassTv.getText().length());

                break;
        }
    }

    private void callChangePasswordApi(String oldPass, String password){
        class ChangePasswordClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(ChangePasswordActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                try {
                    JSONObject reader = new JSONObject(s);

                    //JSONObject sys  = reader.getJSONObject("error");
                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(ChangePasswordActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();

                    } else {

                        Toast.makeText(ChangePasswordActivity.this, "Password update successfully !!!", Toast.LENGTH_LONG).show();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "updatepassword");
                hashMap.put("userid",userId);
                hashMap.put("oldpassword", strings[0]);
                hashMap.put("password",strings[1]);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        ChangePasswordClass changePasswordClass = new ChangePasswordClass();
        changePasswordClass.execute(oldPass,password);
    }
}
