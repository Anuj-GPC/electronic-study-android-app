package com.app.electronicStudy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.MultiPdfGridAdapter;
import com.app.electronicStudy.utils.ResponseMultiListPdf;
import com.app.electronicStudy.utils.ResponsePdfs;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MultiPdfActivity extends AppCompatActivity {
    private GridView multiPdfGridView;
    private MultiPdfGridAdapter multiPdfGridAdapter;
    private ArrayList<ResponseMultiListPdf.ResultEntity> multiPdfList;
    private ResponseMultiListPdf.ResultEntity selectedPdf;

    private TextView headerTitle;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    private String pdfId, pdfName;
    private String userId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_pdf);

        setLayoutId();
        getDataFromIntent();
        setScreenData();
        callgetMultiPdfListApi();

        multiPdfGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPdf = multiPdfList.get(i);
                    Intent intent = new Intent(MultiPdfActivity.this, PdfViewActivity.class);
                    intent.putExtra("pdfFileName", selectedPdf.getPdfFileName());
                    intent.putExtra("pdfFileId", selectedPdf.getParentId());
                    intent.putExtra("isFileType", Constants.FILE_TYPE_MULTIPLE);
                    startActivity(intent);
            }
        });
    }

    private void getDataFromIntent(){
        if(getIntent().getExtras() != null) {
            pdfId = getIntent().getStringExtra("pdfId");
            pdfName = getIntent().getStringExtra("pdfName");
        }
        userId = Utility.getStringValueFromSharedPreference(MultiPdfActivity.this, Constants.USER_ID);
    }

    private void setScreenData(){
        findViewById(R.id.three_dots_img).setVisibility(View.VISIBLE);
        headerTitle.setText(pdfName);

    }

    private void setLayoutId(){
        multiPdfGridView = findViewById(R.id.multi_pdf_grid);
        headerTitle = findViewById(R.id.header_title);
    }

    private void setAdapter(){
        if (multiPdfGridAdapter == null) {
            multiPdfGridAdapter = new MultiPdfGridAdapter(MultiPdfActivity.this, multiPdfList);
            multiPdfGridView.setAdapter(multiPdfGridAdapter);
        } else {
            multiPdfGridAdapter.notifyDataSetChanged();
        }
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_popup, popup.getMenu());
        popup.show();

        String name = Utility.getStringValueFromSharedPreference(this, Constants.USER_NAME);
        if (name != null && name.length() > 0) {
            popup.getMenu().getItem(0).setTitle("Hello " + name);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.one: //Hello User
                        break;
                    case R.id.two: //Edit Password
                        Intent intent = new Intent(MultiPdfActivity.this, ChangePasswordActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.three: //Logout
                        callLogoutApi();
                        break;
                    case R.id.four: //Update Number
                        Intent i = new Intent(MultiPdfActivity.this, UpdateNumberActivity.class);
                        startActivity(i);
                        break;
                }

                return true;
            }
        });
    }


    private void callgetMultiPdfListApi(){
        class PdfClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(MultiPdfActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(MultiPdfActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        multiPdfList = new ArrayList<>();
                        JSONArray pdfArray = reader.getJSONArray("pdfsection");
                        for(int i=0; i<pdfArray.length(); i++){
                            JSONArray pdfDet = new JSONArray();
                            pdfDet = pdfArray.getJSONArray(i);
                            ResponseMultiListPdf.ResultEntity obj = new ResponseMultiListPdf.ResultEntity();
                            obj.setPdfId(pdfDet.getString(0));
                            obj.setParentId(pdfDet.getString(1));
                            obj.setPdfName(pdfDet.getString(2));
                            obj.setPdfFileName(pdfDet.getString(3));
                            multiPdfList.add(obj);
                        }

                        if(multiPdfList.size() == 0){
                            Toast.makeText(MultiPdfActivity.this, "There are no pdfs to be displayed", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            setAdapter();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "pdfsection");
                hashMap.put("pdfid", pdfId);

                String result = httpParse.getRequest(hashMap, Constants.serverUrl);
                return result;
            }
        }
        PdfClass pdfClass = new PdfClass();
        pdfClass.execute();
    }

    private void callLogoutApi(){
        class UserLogoutClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(MultiPdfActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(MultiPdfActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(MultiPdfActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        preferences.edit().clear().commit();

                        Intent i = new Intent(MultiPdfActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "logout");
                hashMap.put("userid",Utility.getStringValueFromSharedPreference(MultiPdfActivity.this, Constants.USER_ID));
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserLogoutClass userLogoutClass = new UserLogoutClass();
        userLogoutClass.execute();
    }
}
