package com.app.electronicStudy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.Utility;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PaymentActivity extends AppCompatActivity {
    PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
    PayUmoneySdkInitializer.PaymentParam paymentParam = null;
    // String txnid ="txt12346";
    String TAG = "PaymentActivity", prodname = "Pdf", merchantId = "6796845", merchantkey = "h4F3Cn3K",
            email, phone, amount, firstname, txnid, pdfId, userId, pdfName, pdfFileType, pdfDisplayName;
    private ProgressDialog progressDialog;
    private HashMap<String, String> hashMap = new HashMap<>();
    private HashMap<String, String> hashMap2 = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    private String hashKeyUrl = Constants.getHashKeyUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        setData();
        startpay();

    }

    private void setData() {
        Intent intent = getIntent();
        amount = intent.getExtras().getString("amount");
        pdfId = intent.getExtras().getString("pdfId");
        pdfName = intent.getExtras().getString("pdfName");
        pdfFileType = intent.getExtras().getString("isFileType");
        pdfDisplayName = intent.getExtras().getString("pdfNameToDisplay");

        email = Utility.getStringValueFromSharedPreference(this, Constants.USER_EMAIL);
        phone = Utility.getStringValueFromSharedPreference(this, Constants.USER_CONTACT);
        firstname = Utility.getStringValueFromSharedPreference(this, Constants.USER_NAME);

        //make txn id
        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        userId = Utility.getStringValueFromSharedPreference(this, Constants.USER_ID);
        txnid = ts + "_" + pdfId + "_" + userId;
        String s;
    }


    public void startpay() {
        builder.setAmount(amount)                          // Payment amount
                .setTxnId(txnid)                     // Transaction ID
                .setPhone(phone)                   // User Phone number
                .setProductName(prodname)                   // Product Name or description
                .setFirstName(firstname)                              // User First name
                .setEmail(email)              // User Email ID
                .setsUrl("https://www.payumoney.com/mobileapp/payumoney/success.php")     // Success URL (surl)
                .setfUrl("https://www.payumoney.com/mobileapp/payumoney/failure.php")     //Failure URL (furl)
                .setUdf1("")
                .setUdf2("")
                .setUdf3("")
                .setUdf4("")
                .setUdf5("")
                .setUdf6("")
                .setUdf7("")
                .setUdf8("")
                .setUdf9("")
                .setUdf10("")
                .setIsDebug(false)                              // Integration environment - true (Debug)/ false(Production)
                .setKey(merchantkey)                        // Merchant key
                .setMerchantId(merchantId);
        try {
            paymentParam = builder.build();
            // generateHashFromServer(paymentParam );
            getHashkey();
        } catch (Exception e) {
            Toast.makeText(PaymentActivity.this, e.toString(), Toast.LENGTH_LONG).show();
            Log.e(TAG, " error s " + e.toString());
        }
    }

    public void getHashkey() {
        class GetHashKeyClass extends AsyncTask<String, Void, String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PaymentActivity.this, "Loading", null, true, true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    String hashKey = s;
                    if (hashKey != null && hashKey.length() > 0) {
                        paymentParam.setMerchantHash(s);
                        PayUmoneyFlowManager.startPayUMoneyFlow(paymentParam, PaymentActivity.this, R.style.AppTheme_default, false);
                    } else {
                        Toast.makeText(PaymentActivity.this, "Could not generate hash", Toast.LENGTH_LONG).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap = new HashMap<>();
                hashMap.put("key", merchantkey);
                hashMap.put("txnid", txnid);//lv
                //hashMap.put("pdfid",pdfId);
                hashMap.put("amount", amount);
                hashMap.put("productinfo", prodname);
                hashMap.put("firstname", firstname);
                hashMap.put("email", email);
                String result = httpParse.postRequest(hashMap, hashKeyUrl);
                return result;
            }
        }
        GetHashKeyClass getHashKeyClass = new GetHashKeyClass();
        getHashKeyClass.execute(email);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
// PayUMoneySdk: Success -- payuResponse{"id":225642,"mode":"CC","status":"success","unmappedstatus":"captured","key":"9yrcMzso","txnid":"223013","transaction_fee":"20.00","amount":"20.00","cardCategory":"domestic","discount":"0.00","addedon":"2018-12-31 09:09:43","productinfo":"a2z shop","firstname":"kamal","email":"kamal.bunkar07@gmail.com","phone":"9144040888","hash":"b22172fcc0ab6dbc0a52925ebbd0297cca6793328a8dd1e61ef510b9545d9c851600fdbdc985960f803412c49e4faa56968b3e70c67fe62eaed7cecacdfdb5b3","field1":"562178","field2":"823386","field3":"2061","field4":"MC","field5":"167227964249","field6":"00","field7":"0","field8":"3DS","field9":" Verification of Secure Hash Failed: E700 -- Approved -- Transaction Successful -- Unable to be determined--E000","payment_source":"payu","PG_TYPE":"AXISPG","bank_ref_no":"562178","ibibo_code":"VISA","error_code":"E000","Error_Message":"No Error","name_on_card":"payu","card_no":"401200XXXXXX1112","is_seamless":1,"surl":"https://www.payumoney.com/sandbox/payment/postBackParam.do","furl":"https://www.payumoney.com/sandbox/payment/postBackParam.do"}
        // Result Code is -1 send from Payumoney activity
        Log.e("PaymentActivity", "request code " + requestCode + " resultcode " + resultCode);
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data != null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager.INTENT_EXTRA_TRANSACTION_RESPONSE);
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    callPaymentSuccessApi();
                } else {
                    //Failure Transaction
                }
                // Response from Payumoney
                String payuResponse = transactionResponse.getPayuResponse();
                // Response from SURl and FURL
                String merchantResponse = transactionResponse.getTransactionDetails();
                Log.e(TAG, "tran " + payuResponse + "---" + merchantResponse);
            }  /*else if (resultModel != null && resultModel.getError() != null) {
                Log.d(TAG, "Error response : " + resultModel.getError().getTransactionResponse());
            } */ else {
               // Toast.makeText(PaymentActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show;
            }
        }
    }


    private void callPaymentSuccessApi(){
        class PaymentSuccessClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PaymentActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                // Toast.makeText(HomeActivity.this, s, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject reader = new JSONObject(s);
                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(PaymentActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                       /* JSONObject user = reader.getJSONObject("user");
                        Utility.putIntValueInSharedPreference(HomeActivity.this, Constants.IS_LOGGED_IN,1);
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_ID, user.getString("id"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_NAME, user.getString("name"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_CONTACT, user.getString("phone"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_EMAIL, user.getString("email"));

                        Intent i = new Intent(HomeActivity.this, CourseActivity.class);
                        startActivity(i);*/
                       Toast.makeText(PaymentActivity.this, "Your payment was successful", Toast.LENGTH_LONG).show();
                        finish();
                        if(pdfFileType.equalsIgnoreCase(Constants.FILE_TYPE_SINGLE)) {
                            Intent intent = new Intent(PaymentActivity.this, PdfViewActivity.class);
                            intent.putExtra("pdfFileName", pdfName);
                            intent.putExtra("pdfFileId", pdfId);
                            intent.putExtra("isFileType", pdfFileType);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(PaymentActivity.this, MultiPdfActivity.class);
                            intent.putExtra("pdfId", pdfId);
                            intent.putExtra("pdfName", pdfDisplayName);
                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap = new HashMap<>();
                hashMap.put("ApiParam", "paymentsuccess");
                hashMap.put("userid",userId);
                hashMap.put("txnid",txnid);
                hashMap.put("pdfid",pdfId);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        PaymentSuccessClass paymentSuccessClass = new PaymentSuccessClass();
        paymentSuccessClass.execute();
    }
}