package com.app.electronicStudy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.Utility;

public class SplashActivity extends AppCompatActivity {

    Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            if(Utility.getIntValueFromSharedPreference(SplashActivity.this, Constants.IS_LOGGED_IN) == 1){
                Intent i = new Intent(SplashActivity.this, CourseActivity.class);
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(i);
                finish();
            }
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setSplashTimeout();
    }

    private void setSplashTimeout(){
        Handler mHandler = new Handler();
        mHandler.postDelayed(mRunnable, 3000);
    }
}
