package com.app.electronicStudy;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class CourseGridAdapter extends BaseAdapter{

        private final LayoutInflater mInflator;
        Context mContext;
        private ArrayList<ResponseCourse.ResultEntity> mCoursesList;

        public CourseGridAdapter(Context context, ArrayList<ResponseCourse.ResultEntity> mData) {

            this.mContext = context;
            this.mCoursesList = mData;
            mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }

        @Override
        public int getCount() {
            return mCoursesList.size();
        }

        @Override
        public ResponseCourse.ResultEntity getItem(int position) {
            return mCoursesList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflator.inflate(R.layout.course_gv_item, null);
                holder = new ViewHolder();
                holder.courseTextView = (TextView) convertView.findViewById(R.id.course_name_tv);
                holder.courseImageView = (ImageView) convertView.findViewById(R.id.course_img);
                holder.gridItem = (RelativeLayout) convertView.findViewById(R.id.course_grid_item);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            ResponseCourse.ResultEntity resultEntity = getItem(position);

            holder.courseTextView.setText(resultEntity.getCourseName());

            return convertView;

        }

        private class ViewHolder {
            private RelativeLayout gridItem;
            private ImageView courseImageView;
            private TextView courseTextView;
        }
}
