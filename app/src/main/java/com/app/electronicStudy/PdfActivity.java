package com.app.electronicStudy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.bluetooth.BluetoothClass;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.ResponsePdfs;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.security.Provider;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PdfActivity extends AppCompatActivity {
    private GridView pdfGridView;
    private PdfGridAdapter pdfGridAdapter;
    private ArrayList<ResponsePdfs.ResultEntity> pdfList;
    private ArrayList<ResponsePdfs.PaymentEntity> paymentList;
    private ResponsePdfs.ResultEntity selectedPdf;
    private ResponsePdfs.PaymentEntity selectedPayment;
    private TextView headerTitle;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    private String subjectId = null;
    private String userId = null;
    //private String selectedCourseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pdf);

        setLayoutId();
        setScreenData();
        getDataFromIntent();
        callgetPdfApi();
        //setAdapter();

        pdfGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPdf = pdfList.get(i);
               // selectedPayment = paymentList.get(i);
                Float price = Float.parseFloat(selectedPdf.getPdfAmount());
                if ( price > 0 ) {
                    callgetPaymentStatusApi();
                } else {
                    if(selectedPdf.getPdfFileType().equalsIgnoreCase(Constants.FILE_TYPE_MULTIPLE)){
                        Intent intent = new Intent(PdfActivity.this, MultiPdfActivity.class);
                        intent.putExtra("pdfId", selectedPdf.getId());
                        intent.putExtra("pdfName", selectedPdf.getPdfNameToDisplay());
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(PdfActivity.this, PdfViewActivity.class);
                        intent.putExtra("pdfFileName", selectedPdf.getPdfName());
                        intent.putExtra("pdfFileId", selectedPdf.getId());
                        intent.putExtra("isFileType", Constants.FILE_TYPE_SINGLE);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void getDataFromIntent(){
        if(getIntent().getExtras() != null) {
            subjectId = getIntent().getStringExtra("subject_id");
        }
        userId = Utility.getStringValueFromSharedPreference(PdfActivity.this, Constants.USER_ID);
    }

    private void setScreenData(){
        findViewById(R.id.three_dots_img).setVisibility(View.VISIBLE);
        headerTitle.setText("PDF");

    }

    private void setLayoutId(){
        pdfGridView = findViewById(R.id.pdf_grid);
        headerTitle = findViewById(R.id.header_title);
    }

    private void setAdapter(){
        if (pdfGridAdapter == null) {
            pdfGridAdapter = new PdfGridAdapter(PdfActivity.this, pdfList, paymentList);
            pdfGridView.setAdapter(pdfGridAdapter);
        } else {
            pdfGridAdapter.notifyDataSetChanged();
        }
    }

    private void showPaymentDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_box);
        dialog.setCancelable(false);
        Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
        Button proceed = (Button) dialog.findViewById(R.id.proceed_btn);
        TextView textView = (TextView) dialog.findViewById(R.id.text_desc);
        String amount = selectedPdf.getPdfAmount();
        textView.setText("You need to purchase this pdf first. AMOUNT: " + amount + " rupees");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sendPurchaseDetails();

              Intent intent = new Intent(PdfActivity.this, PaymentActivity.class);
              intent.putExtra("amount", selectedPdf.getPdfAmount());
              intent.putExtra("pdfId", selectedPdf.getId());
              intent.putExtra("pdfName",selectedPdf.getPdfName());
              intent.putExtra("pdfNameToDisplay",selectedPdf.getPdfNameToDisplay());
              intent.putExtra("isFileType", selectedPdf.getPdfFileType());
              startActivity(intent);

              dialog.dismiss();
            }
        });
        dialog.show();

    }


    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_popup, popup.getMenu());
        popup.show();

        String name = Utility.getStringValueFromSharedPreference(this, Constants.USER_NAME);
        if (name != null && name.length() > 0) {
            popup.getMenu().getItem(0).setTitle("Hello " + name);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.one: //Hello User
                        break;
                    case R.id.two: //Edit Password
                        Intent intent = new Intent(PdfActivity.this, ChangePasswordActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.three: //Logout
                        callLogoutApi();
                        break;
                    case R.id.four: //Update Number
                        Intent i = new Intent(PdfActivity.this, UpdateNumberActivity.class);
                        startActivity(i);
                        break;
                }

                return true;
            }
        });
    }


    private void callgetPdfApi(){
        class PdfClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PdfActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(PdfActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        pdfList = new ArrayList<>();
                        JSONArray pdfArray = reader.getJSONArray("pdfs");
                        for(int i=0; i<pdfArray.length(); i++){
                            JSONArray pdfDet = new JSONArray();
                            pdfDet = pdfArray.getJSONArray(i);
                            ResponsePdfs.ResultEntity obj = new ResponsePdfs.ResultEntity();
                            obj.setId(pdfDet.getString(0));
                            obj.setPdfName(pdfDet.getString(1));
                            obj.setSubjectId(pdfDet.getString(2));
                            obj.setPdfAmount(pdfDet.getString(3));
                            obj.setPdfNameToDisplay(pdfDet.getString(4));
                            obj.setPdfFileType(pdfDet.getString(5));
                            pdfList.add(obj);
                        }
                        paymentList = new ArrayList<>();
                        JSONArray paymentArray = reader.getJSONArray("payment");
                        for (int j = 0; j<paymentArray.length(); j++){
                            JSONArray paymentDet = new JSONArray();
                            paymentDet = paymentArray.getJSONArray(j);
                            ResponsePdfs.PaymentEntity object = new ResponsePdfs.PaymentEntity();
                            object.setPdfId(paymentDet.getString(0));
                            object.setPaymentStatus(paymentDet.getInt(1));
                            paymentList.add(object);
                        }
                        if(pdfList.size() == 0){
                            Toast.makeText(PdfActivity.this, "There are no pdfs in this subject", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            setAdapter();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "pdfs");
                hashMap.put("subjectid", subjectId);
                hashMap.put("userid", userId);

                String result = httpParse.getRequest(hashMap, Constants.serverUrl);
                return result;
            }
        }
        PdfClass pdfClass = new PdfClass();
        pdfClass.execute();
    }


    private void callgetPaymentStatusApi(){
        class PaymentStatusClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PdfActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean purchased = reader.getBoolean("purchased");
                    if (purchased) {
                        if (selectedPdf.getPdfFileType().equalsIgnoreCase(Constants.FILE_TYPE_MULTIPLE)) {
                            Intent intent = new Intent(PdfActivity.this, MultiPdfActivity.class);
                            intent.putExtra("pdfId", selectedPdf.getId());
                            intent.putExtra("pdfName", selectedPdf.getPdfNameToDisplay());
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(PdfActivity.this, PdfViewActivity.class);
                            intent.putExtra("pdfFileName", selectedPdf.getPdfName());
                            intent.putExtra("pdfFileId", selectedPdf.getId());
                            intent.putExtra("isFileType",Constants.FILE_TYPE_SINGLE);
                            startActivity(intent);
                        }
                    } else {
                        showPaymentDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap = new HashMap<>();
                hashMap.put("ApiParam", "pdfbuy");
                hashMap.put("userid", userId);
                hashMap.put("pdfid", selectedPdf.getId());

                String result = httpParse.getRequest(hashMap, Constants.serverUrl);
                return result;
            }
        }
        PaymentStatusClass paymentStatusClass = new PaymentStatusClass();
        paymentStatusClass.execute();
    }

    private void callLogoutApi(){
        class UserLogoutClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(PdfActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(PdfActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(PdfActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        preferences.edit().clear().commit();

                        Intent i = new Intent(PdfActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "logout");
                hashMap.put("userid",Utility.getStringValueFromSharedPreference(PdfActivity.this, Constants.USER_ID));
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserLogoutClass userLogoutClass = new UserLogoutClass();
        userLogoutClass.execute();
    }

}
