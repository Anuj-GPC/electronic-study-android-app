package com.app.electronicStudy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.app.electronicStudy.utils.Constants;

import es.voghdev.pdfviewpager.library.RemotePDFViewPager;
import es.voghdev.pdfviewpager.library.adapter.PDFPagerAdapter;
import es.voghdev.pdfviewpager.library.remote.DownloadFile;


public class PdfViewActivity extends AppCompatActivity implements DownloadFile.Listener{
    LinearLayout root;
    RemotePDFViewPager remotePDFViewPager;
    PDFPagerAdapter adapter;
    //Button btn;
    private String url = null;
    private String urlPath = Constants.getPdfUrl;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        }
        setContentView(R.layout.activity_pdf_view);

        setLayoutId();
        getDataFromIntent();

        if (url != null && url.length() > 0) {
            loadPdf();
        } else {
            Toast.makeText(this, "Something went wrong. Please try again later.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getDataFromIntent(){
        if(getIntent().getExtras() != null){
            String fileType = getIntent().getExtras().getString("isFileType");
            String fileId = getIntent().getExtras().getString("pdfFileId");
            String fileName = getIntent().getExtras().getString("pdfFileName");
            if( fileName != null && fileName.length() > 0){
                if (fileType.equalsIgnoreCase(Constants.FILE_TYPE_MULTIPLE)){
                    url = urlPath + fileId +"/" + fileName;
                } else {
                    url = urlPath + fileName;
                    Log.d("file url", url);
                }
            }
        }
    }

    protected void loadPdf(){
        final Context ctx = this;
        final DownloadFile.Listener listener = this;
        remotePDFViewPager = new RemotePDFViewPager(ctx, url, listener);
        remotePDFViewPager.setId(R.id.pdf_view_pager);

        progressDialog = ProgressDialog.show(PdfViewActivity.this, "Fetching your file. Please wait...",null,true,true);

    }


    public static void open(Context context) {
        Intent i = new Intent(context, PdfViewActivity.class);
        context.startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (adapter != null) {
            adapter.close();
        }
    }

    private void setLayoutId(){
        root = (LinearLayout) findViewById(R.id.root);
        //btn = (Button) findViewById(R.id.button_show_pdf);
    }

    public void updateLayout() {
        root.removeAllViewsInLayout();
       // root.addView(btn,
       //         LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        root.addView(remotePDFViewPager,
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onSuccess(String url, String destinationPath) {
        progressDialog.dismiss();
        adapter = new PDFPagerAdapter(this, extractFileNameFromURL(url));
        remotePDFViewPager.setAdapter(adapter);

        updateLayout();

    }

    public static String extractFileNameFromURL(String url) {
        return url.substring(url.lastIndexOf('/') + 1);
    }

    @Override
    public void onFailure(Exception e) {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        progressDialog.dismiss();

    }

    @Override
    public void onProgressUpdate(int progress, int total) {
    }

    /*@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }*/
}

