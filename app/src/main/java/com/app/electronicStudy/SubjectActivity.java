package com.app.electronicStudy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.ResponseSubject;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import javax.security.auth.Subject;

public class SubjectActivity extends AppCompatActivity {
    private GridView subjectGridView;
    private SubjectGridAdpater subjectGridAdapter;
    private ArrayList<ResponseSubject.ResultEntity> subjectList = new ArrayList<>();
    private TextView headerTitle;
    private String courseId;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);
        setLayoutId();
        setScreenData();
        getDataFromIntent();
        //setAdapter();

        subjectGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SubjectActivity.this, PdfActivity.class);
                intent.putExtra("subject_id", subjectList.get(i).getId());
                startActivity(intent);
            }
        });
    }

    private void setLayoutId(){
        subjectGridView = findViewById(R.id.subject_grid);
        headerTitle = findViewById(R.id.header_title);
    }

    private void setScreenData(){
        findViewById(R.id.three_dots_img).setVisibility(View.VISIBLE);
        headerTitle.setText("SUBJECTS");
    }

    private void setAdapter(){
        if (subjectGridAdapter == null) {
            subjectGridAdapter = new SubjectGridAdpater(SubjectActivity.this, subjectList);
            subjectGridView.setAdapter(subjectGridAdapter);
        } else {
            subjectGridAdapter.notifyDataSetChanged();
        }
    }

    private void getDataFromIntent(){
        if(getIntent().getExtras() != null){
            courseId = getIntent().getExtras().getString("course_id");
            callGetSubjectListApi();
        }
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_popup, popup.getMenu());
        popup.show();

        String name = Utility.getStringValueFromSharedPreference(this, Constants.USER_NAME);
        if (name != null && name.length() > 0) {
            popup.getMenu().getItem(0).setTitle("Hello " + name);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.one: //Hello User
                        break;
                    case R.id.two: //Edit Password
                        Intent intent = new Intent(SubjectActivity.this, ChangePasswordActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.three: //Logout
                        callLogoutApi();
                        break;
                    case R.id.four: //Update Number
                        Intent i = new Intent(SubjectActivity.this, UpdateNumberActivity.class);
                        startActivity(i);
                        break;
                }

                return true;
            }
        });
    }

    private void callGetSubjectListApi(){
        class SubjectClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(SubjectActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                try {
                    JSONObject reader = new JSONObject(s);

                    //JSONObject sys  = reader.getJSONObject("error");
                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(SubjectActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        subjectList = new ArrayList<>();
                        JSONArray subArray = reader.getJSONArray("subjects");
                        for(int i=0; i<subArray.length(); i++){
                            JSONArray subDet = new JSONArray();
                            subDet = subArray.getJSONArray(i);
                            ResponseSubject.ResultEntity obj = new ResponseSubject.ResultEntity();
                            obj.setId(subDet.getString(0));
                            obj.setSubjectName(subDet.getString(1));
                            obj.setCourseId(subDet.getString(2));
                            subjectList.add(obj);

                        }
                        if(subjectList.size() == 0){
                            Toast.makeText(SubjectActivity.this, "There are no subjects in this course", Toast.LENGTH_LONG).show();
                            finish();
                        } else {
                            setAdapter();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "subjects");
                hashMap.put("courseid", courseId);
                String result = httpParse.getRequest(hashMap, Constants.serverUrl);
                return result;
            }
        }
        SubjectClass subjectClass = new SubjectClass();
        subjectClass.execute();
    }

    private void callLogoutApi(){
        class UserLogoutClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(SubjectActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(SubjectActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(SubjectActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        preferences.edit().clear().commit();

                        Intent i = new Intent(SubjectActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "logout");
                hashMap.put("userid",Utility.getStringValueFromSharedPreference(SubjectActivity.this, Constants.USER_ID));
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserLogoutClass userLogoutClass = new UserLogoutClass();
        userLogoutClass.execute();
    }
}
