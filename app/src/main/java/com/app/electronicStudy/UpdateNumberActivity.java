package com.app.electronicStudy;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class UpdateNumberActivity extends AppCompatActivity {
    EditText updateNumEt;
    Button updateBtn;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_number);

        setLayoutId();
        setData();

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateDataForUpdateNumber()){
                    callUpdateNumberApi(updateNumEt.getText().toString());
                }
            }
        });
    }

    private void setLayoutId(){
        updateNumEt = (EditText) findViewById(R.id.update_number_et);
        updateBtn = (Button) findViewById(R.id.update_number_btn);
    }

    private void setData(){
        String num = Utility.getStringValueFromSharedPreference(this, Constants.USER_CONTACT);
        if(num != null && num.length() > 0){
            updateNumEt.setText(num);
        }
        userId = Utility.getStringValueFromSharedPreference(this, Constants.USER_ID);
    }

    private boolean validateDataForUpdateNumber() {
        if (TextUtils.isEmpty(updateNumEt.getText().toString())) {
            Toast.makeText(this, "Mobile number is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(Patterns.PHONE.matcher(updateNumEt.getText().toString()).matches()) || updateNumEt.getText().toString().length() != 10){
            Toast.makeText(this, "Please enter a valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }
        /*
        else if (!(Patterns.EMAIL_ADDRESS.matcher(newEmailEt.getText().toString()).matches())){
            Toast.makeText(this, "Please enter a valid email id", Toast.LENGTH_SHORT).show();
            return false;
        }
         */
        return true;
    }

    private void callUpdateNumberApi(final String number){
        class UserUpdateNumberClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(UpdateNumberActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);
                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        Toast.makeText(UpdateNumberActivity.this, message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(UpdateNumberActivity.this, "Your number has been updated successfully",Toast.LENGTH_LONG).show();
                        String phone = reader.getString("phone");
                        Utility.putStringValueInSharedPreference(UpdateNumberActivity.this, Constants.USER_CONTACT,phone);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "updatephone");
                hashMap.put("userid",userId);
                hashMap.put("phone",number);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserUpdateNumberClass userUpdateNumberClass = new UserUpdateNumberClass();
        userUpdateNumberClass.execute(number);
    }
}
