package com.app.electronicStudy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.electronicStudy.utils.ResponsePdfs;

import java.util.ArrayList;

/**
 * Created by srishti on 24/08/19.
 */

public class PdfGridAdapter extends BaseAdapter {
    private final LayoutInflater mInflator;
    Context mContext;
    private ArrayList<ResponsePdfs.ResultEntity> mPdfsList;
    private ArrayList<ResponsePdfs.PaymentEntity> paymentData;

    public PdfGridAdapter(Context context, ArrayList<ResponsePdfs.ResultEntity> mData, ArrayList<ResponsePdfs.PaymentEntity> paymentData) {

        this.mContext = context;
        this.mPdfsList = mData;
        this.paymentData = paymentData;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mPdfsList.size();
    }

    @Override
    public ResponsePdfs.ResultEntity getItem(int position) {
        return mPdfsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.pdf_gv_item, null);
            holder = new ViewHolder();
            holder.pdfTextView = (TextView) convertView.findViewById(R.id.pdf_name_tv);
            holder.pdfImageView = (ImageView) convertView.findViewById(R.id.pdf_img);
            //holder.gridItem = (RelativeLayout) convertView.findViewById(R.id.pdf_grid_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ResponsePdfs.ResultEntity resultEntity = getItem(position);

        holder.pdfTextView.setText(resultEntity.getPdfNameToDisplay());

        return convertView;

    }

    private class ViewHolder {
        //private RelativeLayout gridItem;
        private ImageView pdfImageView;
        private TextView pdfTextView;
    }
}
