package com.app.electronicStudy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.electronicStudy.utils.ResponseSubject;

import java.util.ArrayList;

/**
 * Created by srishti on 08/08/19.
 */

public class SubjectGridAdpater extends BaseAdapter {
    private final LayoutInflater mInflator;
    Context mContext;
    private ArrayList<ResponseSubject.ResultEntity> mSubjectList;

    public SubjectGridAdpater(Context context, ArrayList<ResponseSubject.ResultEntity> mData) {

        this.mContext = context;
        this.mSubjectList = mData;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return mSubjectList.size();
    }

    @Override
    public ResponseSubject.ResultEntity getItem(int position) {
        return mSubjectList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        SubjectGridAdpater.ViewHolder holder;

        if (convertView == null) {
            convertView = mInflator.inflate(R.layout.subject_gv_item, null);
            holder = new SubjectGridAdpater.ViewHolder();
            holder.subjectTextView = (TextView) convertView.findViewById(R.id.subject_name_tv);
            holder.subjectImg = (ImageView) convertView.findViewById(R.id.subject_img);
            convertView.setTag(holder);
        } else {
            holder = (SubjectGridAdpater.ViewHolder) convertView.getTag();
        }

        ResponseSubject.ResultEntity resultEntity = getItem(position);
        holder.subjectTextView.setText(resultEntity.getSubjectName());

        return convertView;

    }

    private class ViewHolder {
        private RelativeLayout gridItem;
        private ImageView subjectImg;
        private TextView subjectTextView;
    }
}
