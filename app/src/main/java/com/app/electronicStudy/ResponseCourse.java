package com.app.electronicStudy;

/**
 * Created by srishti on 07/08/19.
 */

public class ResponseCourse {

    public static class ResultEntity {
        private String id;
        private String courseName;

        public void setId(String id){this.id = id;}
        public String getId(){return id;}

        public void setCourseName(String courseName){this.courseName = courseName;}
        public String getCourseName(){return  courseName;}
    }
}
