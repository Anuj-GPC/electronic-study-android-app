package com.app.electronicStudy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CourseActivity extends AppCompatActivity {
    private GridView courseGridView;
    private CourseGridAdapter courseGridAdapter;
    private ArrayList<ResponseCourse.ResultEntity> courseList;
    private TextView headerTitle;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    HttpParse httpParse = new HttpParse();
    private String selectedCourseId;
    private int closeApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);

        setLayoutId();
        setScreenData();
        callgetCoursesApi();
        //setAdapter();

        courseGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Toast.makeText(CourseActivity.this, "You have selected course " + (i+1), Toast.LENGTH_SHORT).show();
                //Intent intent = new Intent(CourseActivity.this, SubjectActivity.class);
                //startActivity(intent);
                selectedCourseId = courseList.get(i).getId();

                Intent intent = new Intent(CourseActivity.this, SubjectActivity.class);
                intent.putExtra("course_id", selectedCourseId);
                startActivity(intent);
            }
        });
    }

    private void setScreenData(){
        findViewById(R.id.three_dots_img).setVisibility(View.VISIBLE);
        headerTitle.setText("COURSES");

    }

    private void setLayoutId(){
        courseGridView = findViewById(R.id.courses_grid);
        headerTitle = findViewById(R.id.header_title);
    }

    private void setAdapter(){
        if (courseGridAdapter == null) {
            courseGridAdapter = new CourseGridAdapter(CourseActivity.this, courseList);
            courseGridView.setAdapter(courseGridAdapter);
        } else {
            courseGridAdapter.notifyDataSetChanged();
        }
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_popup, popup.getMenu());
        popup.show();

        String name = Utility.getStringValueFromSharedPreference(this, Constants.USER_NAME);
        if (name != null && name.length() > 0) {
            popup.getMenu().getItem(0).setTitle("Hello " + name);
        }

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.one: //Hello User
                        break;
                    case R.id.two: //Edit Password
                        Intent intent = new Intent(CourseActivity.this, ChangePasswordActivity.class);
                         startActivity(intent);
                        break;
                    case R.id.three: //Logout
                         callLogoutApi();
                        break;
                    case R.id.four: //Update Number
                        Intent i = new Intent(CourseActivity.this, UpdateNumberActivity.class);
                        startActivity(i);
                        break;
                }

                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
        if(closeApp == 0) {
            showAppCloseDialog();
        } else {
            super.onBackPressed();
        }
    }

    private void showAppCloseDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_box);
        dialog.setCancelable(false);
        Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
        Button proceed = (Button) dialog.findViewById(R.id.proceed_btn);
        TextView textView = (TextView) dialog.findViewById(R.id.text_desc);

        textView.setText("Are you sure you want to exit Electronic Study?");
        cancel.setText("NO");
        proceed.setText("YES");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeApp = 1;
                onBackPressed();
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    private void callgetCoursesApi(){
        class CoursesClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(CourseActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                //Toast.makeText(CourseActivity.this, s, Toast.LENGTH_SHORT).show();
                try {
                    JSONObject reader = new JSONObject(s);

                    //JSONObject sys  = reader.getJSONObject("error");
                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(CourseActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        courseList = new ArrayList<>();
                        JSONArray coursesArray = reader.getJSONArray("course");
                        for(int i=0; i<coursesArray.length(); i++){
                            JSONArray courseDet = new JSONArray();
                            courseDet = coursesArray.getJSONArray(i);
                            ResponseCourse.ResultEntity obj = new ResponseCourse.ResultEntity();
                            obj.setId(courseDet.getString(0));
                            obj.setCourseName(courseDet.getString(1));
                            courseList.add(obj);

                        }

                        setAdapter();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "course");
                String result = httpParse.getRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        CoursesClass coursesClass = new CoursesClass();
        coursesClass.execute();
    }

    private void callLogoutApi(){
        class UserLogoutClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(CourseActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(CourseActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(CourseActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                        preferences.edit().clear().commit();
                        Intent i = new Intent(CourseActivity.this, HomeActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "logout");
                hashMap.put("userid",Utility.getStringValueFromSharedPreference(CourseActivity.this, Constants.USER_ID));
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserLogoutClass userLogoutClass = new UserLogoutClass();
        userLogoutClass.execute();
    }
}
