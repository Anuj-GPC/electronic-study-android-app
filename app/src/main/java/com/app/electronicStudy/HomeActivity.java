package com.app.electronicStudy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.electronicStudy.network.HttpParse;
import com.app.electronicStudy.utils.Constants;
import com.app.electronicStudy.utils.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText nameEt, newEmailEt, newPhoneNumberEt, newPassEt, reEnterPassEt, emailEt, passwordEt, email;
    private CheckBox passwordChk, newPasswordChk, reenterPasswordChk;
    private ProgressDialog progressDialog;
    private HashMap<String,String> hashMap = new HashMap<>();
    private int closeApp;
    HttpParse httpParse = new HttpParse();
    //http://localhost/electronicStudy/api/electronic_study_api.php?ApiParam=signup&name=xyz&email=xyz@gmail.com&phone=111&password=qqq
    //http://electronicstudy.epizy.com/src/api/electronic_study_api.php?ApiParam=signup&name=xyz&email=xyz@gmail.com&phone=111&password=qqq


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setLayoutIds();
        setOnClickListeners();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signin_btn:
                //callgetCoursesApi();
                if(validateDataForSignin()){
                    callUserLoginApi(emailEt.getText().toString(), passwordEt.getText().toString());
                }
                break;
            case R.id.signup_btn:
                if(validateDataForSignup()){
                    callUserRegistrationApi(nameEt.getText().toString(),newEmailEt.getText().toString(),newPhoneNumberEt.getText().toString(),newPassEt.getText().toString());
                }
                break;
            case R.id.signin_tv:
                findViewById(R.id.signup_layout).setVisibility(View.GONE);
                findViewById(R.id.signin_layout).setVisibility(View.VISIBLE);
                break;
            case R.id.signup_tv:
                findViewById(R.id.signin_layout).setVisibility(View.GONE);
                findViewById(R.id.signup_layout).setVisibility(View.VISIBLE);
                break;
            case R.id.password_chk:
                if(passwordChk.isChecked()){
                    passwordEt.setTransformationMethod(new HideReturnsTransformationMethod());
                }
                else{
                    passwordEt.setTransformationMethod(new PasswordTransformationMethod());
                }
                passwordEt.setSelection(passwordEt.getText().length());

                break;
            case R.id.new_password_chk:
                if(newPasswordChk.isChecked()){
                    newPassEt.setTransformationMethod(new HideReturnsTransformationMethod());
                }
                else{
                    newPassEt.setTransformationMethod(new PasswordTransformationMethod());
                }
                newPassEt.setSelection(newPassEt.getText().length());

                break;
            case R.id.reenter_new_password_chk:
                if(reenterPasswordChk.isChecked()){
                    reEnterPassEt.setTransformationMethod(new HideReturnsTransformationMethod());
                }
                else{
                    reEnterPassEt.setTransformationMethod(new PasswordTransformationMethod());
                }
                reEnterPassEt.setSelection(reEnterPassEt.getText().length());

                break;
            case R.id.forgot_pass:
                showForgotPassDialog();
                break;
          /*  case R.id.logout_all:
                showLogoutFromAllDevicesDialog();
                break;*/
        }
    }

    private void showForgotPassDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_pass_dialog);

        Button forgotPass = (Button) dialog.findViewById(R.id.send_pass__btn);
        email = (EditText) dialog.findViewById(R.id.forgotpass_email_et) ;

        forgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validateDataForForgotPass()){
                    callForgotPasswordApi(email.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();

    }

    private void showLogoutFromAllDevicesDialog(){
        final  Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.forgot_pass_dialog);

        Button logoutAll = (Button) dialog.findViewById(R.id.send_pass__btn);
        email = (EditText) dialog.findViewById(R.id.forgotpass_email_et) ;
        logoutAll.setText("LOGOUT");

        logoutAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateDataForForgotPass()){
                    callLogoutFromAllDevicesApi(email.getText().toString());
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void setOnClickListeners(){
        findViewById(R.id.signin_btn).setOnClickListener(this);
        findViewById(R.id.signup_btn).setOnClickListener(this);
        findViewById(R.id.signin_tv).setOnClickListener(this);
        findViewById(R.id.signup_tv).setOnClickListener(this);
        findViewById(R.id.forgot_pass).setOnClickListener(this);
        //findViewById(R.id.logout_all).setOnClickListener(this);
        passwordChk.setOnClickListener(this);
        newPasswordChk.setOnClickListener(this);
        reenterPasswordChk.setOnClickListener(this);
    }

    private void  setLayoutIds(){
        nameEt = (EditText) findViewById(R.id.name_et);
        newEmailEt = (EditText) findViewById(R.id.new_email_et);
        newPhoneNumberEt = (EditText) findViewById(R.id.phone_et);
        newPassEt = (EditText) findViewById(R.id.new_password_et);
        reEnterPassEt = (EditText) findViewById(R.id.reenter_new_password_et);
        emailEt = (EditText) findViewById(R.id.email_et);
        passwordEt = (EditText) findViewById(R.id.password_et);
        passwordChk =  (CheckBox) findViewById(R.id.password_chk);
        newPasswordChk = (CheckBox) findViewById(R.id.new_password_chk);
        reenterPasswordChk = (CheckBox) findViewById(R.id.reenter_new_password_chk);
    }

    private boolean validateDataForSignup() {
        if (TextUtils.isEmpty(newEmailEt.getText().toString())){
            Toast.makeText(this, "Email address is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(Patterns.EMAIL_ADDRESS.matcher(newEmailEt.getText().toString()).matches())){
            Toast.makeText(this, "Please enter a valid email id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(newPassEt.getText().toString())){
            Toast.makeText(this, "Password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (newPassEt.getText().toString().length() < 6){
            Toast.makeText(this, "Password should be of minimum 6 characters", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(newPhoneNumberEt.getText().toString())){
            Toast.makeText(this, "Phone number is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        }else if (!(Patterns.PHONE.matcher(newPhoneNumberEt.getText().toString()).matches()) || newPhoneNumberEt.getText().toString().length() != 10){
            Toast.makeText(this, "Please enter a valid mobile number", Toast.LENGTH_SHORT).show();
            return false;
        }else if (TextUtils.isEmpty(reEnterPassEt.getText().toString())){
            Toast.makeText(this, "Re entering password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(reEnterPassEt.getText().toString().equals(newPassEt.getText().toString()))) {
            Toast.makeText(this, "Passwords dont match", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callUserRegistrationApi(String name,String email,String phone,String password){
        class UserRegistrationFunctionClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(HomeActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();

                try {
                    JSONObject reader = new JSONObject(s);

                    //JSONObject sys  = reader.getJSONObject("error");
                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
                        }

                    } else {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                        JSONObject user = reader.getJSONObject("user");
                        //Utility.putIntValueInSharedPreference(HomeActivity.this, Constants.IS_LOGGED_IN,1);
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_ID, user.getString("id"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_NAME, user.getString("name"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_CONTACT, user.getString("phone"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_EMAIL, user.getString("email"));

                        findViewById(R.id.signup_layout).setVisibility(View.GONE);
                        findViewById(R.id.signin_layout).setVisibility(View.VISIBLE);

                       /* Intent i = new Intent(HomeActivity.this, CourseActivity.class);
                        startActivity(ia);
                        finish();*/
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "signup");
                hashMap.put("name",strings[0]);
                hashMap.put("email",strings[1]);
                hashMap.put("phone",strings[2]);
                hashMap.put("password",strings[3]);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserRegistrationFunctionClass userRegistrationFunctionClass = new UserRegistrationFunctionClass();
        userRegistrationFunctionClass.execute(name,email,phone,password);
    }

    private boolean validateDataForSignin() {
        if (TextUtils.isEmpty(emailEt.getText().toString())){
            Toast.makeText(this, "Email address is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(Patterns.EMAIL_ADDRESS.matcher(emailEt.getText().toString()).matches())){
            Toast.makeText(this, "Please enter a valid email id", Toast.LENGTH_SHORT).show();
            return false;
        } else if (TextUtils.isEmpty(passwordEt.getText().toString())){
            Toast.makeText(this, "Password is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateDataForForgotPass() {
        if (TextUtils.isEmpty(email.getText().toString())) {
            Toast.makeText(this, "Email address is mandatory", Toast.LENGTH_SHORT).show();
            return false;
        } else if (!(Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches())){
            Toast.makeText(this, "Please enter a valid email id", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void callUserLoginApi(String email, String password){
        class UserLoginFunctionClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(HomeActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);

                    //JSONObject sys  = reader.getJSONObject("error");
                    Boolean error = reader.getBoolean("error");
                    String message = reader.getString("message");
                    if (error) {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if(message != null && message.length() > 0) {
                            Toast.makeText(HomeActivity.this, message, Toast.LENGTH_LONG).show();
                        }
                        JSONObject user = reader.getJSONObject("user");
                        Utility.putIntValueInSharedPreference(HomeActivity.this, Constants.IS_LOGGED_IN,1);
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_ID, user.getString("id"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_NAME, user.getString("name"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_CONTACT, user.getString("phone"));
                        Utility.putStringValueInSharedPreference(HomeActivity.this, Constants.USER_EMAIL, user.getString("email"));

                        Intent i = new Intent(HomeActivity.this, CourseActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "login");
                hashMap.put("email",strings[0]);
                hashMap.put("password",strings[1]);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserLoginFunctionClass userLoginFunctionClass = new UserLoginFunctionClass();
        userLoginFunctionClass.execute(email,password);
    }

    private void callForgotPasswordApi(String email){
        class UserForgotPasswordClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(HomeActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);
                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(HomeActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(HomeActivity.this, "Your password has been sent to the entered email id. Please check there.",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "forgetpassword");
                hashMap.put("email",strings[0]);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserForgotPasswordClass userForgotPasswordClass = new UserForgotPasswordClass();
        userForgotPasswordClass.execute(email);
    }

    private void callLogoutFromAllDevicesApi(String email){
        class UserLogoutAllClass extends AsyncTask<String,Void,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = ProgressDialog.show(HomeActivity.this, "Loading",null,true,true);

            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                try {
                    JSONObject reader = new JSONObject(s);
                    Boolean error = reader.getBoolean("error");
                    if (error) {
                        Toast.makeText(HomeActivity.this, reader.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(HomeActivity.this, reader.getString("message"),Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(String... strings) {
                hashMap.put("ApiParam", "logoutalldevices");
                hashMap.put("email",strings[0]);
                String result = httpParse.postRequest(hashMap,Constants.serverUrl);
                return result;
            }
        }
        UserLogoutAllClass userLogoutAllClass = new UserLogoutAllClass();
        userLogoutAllClass.execute(email);
    }

    @Override
    public void onBackPressed() {
        if(closeApp == 0) {
            showAppCloseDialog();
        } else {
            super.onBackPressed();
        }
    }

    private void showAppCloseDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_box);
        dialog.setCancelable(false);
        Button cancel = (Button) dialog.findViewById(R.id.cancel_btn);
        Button proceed = (Button) dialog.findViewById(R.id.proceed_btn);
        TextView textView = (TextView) dialog.findViewById(R.id.text_desc);

        textView.setText("Are you sure you want to exit Electronic Study?");
        cancel.setText("NO");
        proceed.setText("YES");

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeApp = 1;
                onBackPressed();
                dialog.dismiss();

            }
        });
        dialog.show();
    }
}
